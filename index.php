<?php
    include("connection.php");

    //Nacitani clanku
    //Clanky jsou podle ID, protoze clanek s vyssim ID je nejnovejsi
    $query = "SELECT * FROM `articles` ORDER BY `id` DESC";
    $result = mysqli_query($link, $query);

    if(mysqli_num_rows($result) == 0) {
        $answer = "Zadny clanek neni k dispozici.";
    } else {
        $answer = "";
    }

    include("header.php");

?>
<div class="seznam-clanku-pozadi">

<main>

    <div class="container">
        <h1>Blog</h1>

        <div class="seznam-clanku">
            <span>Seznam clanku</span>

            <?php

                echo $answer;

                while($row = mysqli_fetch_array($result)) {
                    echo "<div class='clanky'>";

                    echo "<a href='article.php?article_id=".$row['id']."'>".$row['title']."</a>";

                    echo "</div>";

                }

            ?>
        </div>

    </div>

</main>
</div>

<?php include("footer.php"); ?>
