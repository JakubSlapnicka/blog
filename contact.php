<?php

    include("header.php");

?>

    <div class="container">

        <h2>Kontakt</h2>

        <p>
            Potrebujete pomoci s cimkoliv ohledne nasich clanku? Pište na e-mail <a href="mailto:info@primat.cz">info@primat.cz</a>
        </p>

    </div>

<?php include("footer.php"); ?>