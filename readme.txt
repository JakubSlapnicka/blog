PHP 7.2.10
MySQL 5.7.23

1. Nejprve si st�hneme WampServer, kter� slou�� jako server pro projekt.
(Odkaz na WampServer: http://www.wampserver.com/en/)

2. Po instalaci si st�hnem projekt z git reposit��e bitbucket
(Odkaz na reposit��: https://bitbucket.org/JakubSlapnicka/blog/src/master/)

3. Projekt mus�me p�esunout na n� "server". To ud�l�me tak, �e si najedeme do slo�ky C:\wamp64\www
(nebo wamp32 pokud jste si nainstalovali 32 bitovou verzi) a zde ulo��me n� projekt.

4. WampServer obsahuje spr�vce phpMyAdmin a ten si otev�eme tak, �e do prohl�e�e zad�me localhost/phpmyadmin/ a zde si vytvo��me
novou datab�zi "articles_db".

5. Po vytvo�en� datab�ze klikneme na "Import" a pomoc� souboru "articles_db.sql", kter� se nach�z� v na�em projektu,
importujeme datab�zi z projektu.

6. V projektu je soubor "connection.php". Ten si otev�eme a nastav�me "user" a "password".

7. Nyn� je projekt funk�n� a m��eme si ho zobrazit, kdy� zad�me do prohl�e�e localhost/blog/index.php