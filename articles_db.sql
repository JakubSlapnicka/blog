-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1:3306
-- Vytvořeno: Pon 11. úno 2019, 10:23
-- Verze serveru: 5.7.23
-- Verze PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `articles_db`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `perex` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `title`, `perex`, `content`) VALUES
(1, 'Article One', 'perex', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p>'),
(2, 'Article Two', 'perex', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p>'),
(3, 'Article Three', 'perex', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p>'),
(4, 'Article Four', 'perex', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Nullam sit amet magna in magna gravida vehicula. Suspendisse sagittis ultrices augue. Curabitur vitae diam non enim vestibulum interdum. Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. Duis condimentum augue id magna semper rutrum. In rutrum. Aliquam erat volutpat. Vestibulum fermentum tortor id mi. Aliquam id dolor. Integer imperdiet lectus quis justo. Pellentesque arcu. Nullam at arcu a est sollicitudin euismod. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.</p>');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
